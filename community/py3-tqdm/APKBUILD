# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=py3-tqdm
_pkgname=tqdm
pkgver=4.62.0
pkgrel=0
pkgdesc="Fast, Extensible Progress Meter"
options="!check" # Broken on our current Python build due to setuptools version conflict
url="https://pypi.python.org/pypi/tqdm"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-tqdm" # Backwards compatibility
provides="py-tqdm=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare
	case "$CARCH" in
		# FIXME: remove selected failing tests on selected arches
		aarch64) rm tests/tests_perf.py;;
		s390x) rm tests/tests_perf.py;;
		x86) rm tests/tests_synchronisation.py;;
	esac

	# Remove dep on py3-setuptools_scm
	sed -e "s|use_scm_version=True|version='$pkgver'|" \
		-i setup.py
	sed -e '/setuptools_scm/d' -i setup.cfg
}

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	mv "$pkgdir"/usr/bin/tqdm "$pkgdir"/usr/bin/tqdm-3

	mkdir -p "$pkgdir"/usr/bin
	ln -s tqdm-3 "$pkgdir"/usr/bin/tqdm
}

sha512sums="
dd60d5522b68086410ec5f02574477dcc33a887ef509cc1d1735921dd906f951180d6115680517b3abbd2cf6c4ee1c60b9c950654ea9b7642d52d28052326088  tqdm-4.62.0.tar.gz
"
