# Contributor: Milan P. Stanić <mps@arvanta.net>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Milan P. Stanić <mps@arvanta.net>
pkgname=iwd
pkgver=1.15
pkgrel=1
pkgdesc="Internet Wireless Daemon"
url="https://iwd.wiki.kernel.org/"
arch="all"
license="LGPL-2.1-or-later"
depends="dbus"
makedepends="dbus-dev readline-dev linux-headers"
options="!check" # some builders fail on some test
checkdepends="coreutils"
subpackages="
	$pkgname-doc
	$pkgname-openrc
	ead
	ead-openrc:ead_openrc:noarch
	"
source="https://mirrors.edge.kernel.org/pub/linux/network/wireless/iwd-$pkgver.tar.gz
	dbus-netdev-group.patch
	iwd.initd
	iwd.confd
	ead.initd
	ead.confd
	main.conf
	"

case "$CARCH" in
mips*)	options="!check";;
esac

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-systemd-service \
		--enable-sim-hardcoded \
		--enable-wired \
		--enable-tools
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -m750 -d "$pkgdir"/var/lib/$pkgname
	install -m644 -D "$srcdir"/main.conf "$pkgdir"/etc/$pkgname/main.conf

	install -m755 -D "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -m644 -D "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
}

ead() {
	pkgdesc="Ethernet authentication daemon"

	amove usr/libexec/ead
	amove usr/share/dbus-1/system.d/ead-dbus.conf

	install -m750 -d "$subpkgdir"/etc/ead
	# ead expects configs in /var/lib/ead, but these are really configs,
	# not dynamically generated, so they should be in /etc.
	mkdir -p "$subpkgdir"/var/lib
	ln -s /etc/ead "$subpkgdir"/var/lib/ead
}

ead_openrc() {
	pkgdesc="Ethernet authentication daemon (OpenRC init scripts)"
	depends=""
	install_if="openrc ead=$pkgver-r$pkgrel"

	install -m755 -D "$srcdir"/ead.initd "$subpkgdir"/etc/init.d/ead
	install -m644 -D "$srcdir"/ead.confd "$subpkgdir"/etc/conf.d/ead
}

sha512sums="
398080ead7194bbfe4db4b292ee866c926a515b0b87e3c6afa96ae1949706caafb59081eef81ed42d227f5d0beb075b65dbb392f6af8d10d3978e373b28c3a29  iwd-1.15.tar.gz
7d3bc26b558ebfd22335b946f09abd5326e885275979c617af7def1468ade23ba7605f3b13aaf91836035c130aaec04be0ff2708a898f3ae835e0eef4e78fa0e  dbus-netdev-group.patch
fdad00af35c688c9575a629b28d36a72e4aca0f69adeccff2618f92dc03126c7bcf096b5eec10fcee10b2a176942fa994b5fc5acd4c00c38306849c5cb29fc39  iwd.initd
edd81077b9b3b8aa98da71c0f318efb23432d9ff81484718d48f8edc4e31bd51d17caa7f9b87a0bbda7c7f015804d80bbdb612627f06a86f435a82eee9e302e2  iwd.confd
509401a385476b2491f175893013093217351409729ebf3746b8c4cb33c5b6c812fd5ca3d85881a520ad40696c414dfb3a0b585b5dd937c9437142677dc49492  ead.initd
c44abe2943b7befea0afa2bad55cc022078c2c3103bb99028a5f0e9210ecb9c7b746b339fc15ab866ee8664ccc2c226e2776eeb8e4288da83cb7427adf7a6cd2  ead.confd
09f4097e653cfecfa1e4bc7b4843832785d0a8ef2a8ecf7daa3d5be704d9ac311fbbecf9f9f5b8b1c75beff894e4cf0d8fda4d6ff728a638dedf77aff0197179  main.conf
"
