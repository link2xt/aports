# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
#
# TODO: Link with system-provided openssl library.
pkgname=xh
pkgver=0.11.0
pkgrel=0
pkgdesc="Yet another HTTPie clone"
url="https://github.com/ducaale/xh"
# ppc64le fails to build
# others are limited by rust/cargo
arch="x86_64 armv7 armhf aarch64 x86"
license="MIT"
checkdepends="curl-dev openssl-dev"
makedepends="cargo oniguruma-dev zlib-dev"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/ducaale/xh/archive/v$pkgver/$pkgname-$pkgver.tar.gz
	minimize-size.patch
	without-static-ssl.patch
	"

# Link with system-provided oniguruma library (instead building from source
# and bundling).
export RUSTONIG_DYNAMIC_LIBONIG=1

build() {
	cargo build --release --locked
}

check() {
	cargo test --locked
}

package() {
	cargo install --locked --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*

	ln -s xh "$pkgdir"/usr/bin/xhs

	install -Dm 644 doc/xh.1 "$pkgdir"/usr/share/man/man1/xh.1

	install -Dm 644 completions/xh.bash "$pkgdir"/usr/share/bash-completion/completions/xh
	install -Dm 644 completions/xh.fish "$pkgdir"/usr/share/fish/completions/xh.fish
	install -Dm 644 completions/_xh "$pkgdir"/usr/share/zsh/site-functions/_xh
}

sha512sums="
2fb1d9d64f0b9a4649aa075ca5448cce14c89c9583efbbaf199a12e924f02db4197765a641cf96f14bbf871138e3c3070733f2abf747fd86ff9e10e09fcbc307  xh-0.11.0.tar.gz
6cbd742b0c84a5c16fe3cf4e277c2c310624055c6e6fbcd2d7e47961247ac6ca0311dbfb158e33d1e065ef8d632c214efcc01dd5f0814405872ebb41d0a15001  minimize-size.patch
91449f02ea556cd25b0cea0a675d638b2f9efe6685469403a60f7baa5b1c36b6be75814bb7e1608458765adbf72544dd4dea4345b5e92f3837a19c485693ec9f  without-static-ssl.patch
"
